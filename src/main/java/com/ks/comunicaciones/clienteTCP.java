package com.ks.comunicaciones;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.EventosTCP;
import com.ks.lib.tcp.Tcp;

/**
 * Created by migue on 16/10/2015.
 */
public class clienteTCP extends Cliente implements EventosTCP
{


    private int VMintContador;

    public clienteTCP()
    {
        VMintContador = 0;
        this.setEventos(this);
    }

    public void conexionEstablecida(Cliente cliente)
    {
        System.out.println("Se conecto con el servidor");
    }

    public void errorConexion(String s)
    {
        System.out.println("Problema al conectarse con el cliente: " + s);
    }

    public void datosRecibidos(java.lang.String s, byte[] bytes, Tcp tcp)
    {
        System.out.println("Mensaje recibido " + VMintContador);
        System.out.println(s);
    }

    public void cerrarConexion(Cliente cliente)
    {

    }
}
