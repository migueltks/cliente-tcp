package com.ks.comunicaciones;

public class App
{
    public static void main(String[] args)
    {
        if (args.length >= 3)
        {
            clienteTCP cliente = new clienteTCP();
            cliente.setIP(args[0]);
            cliente.setPuerto(Integer.parseInt(args[1]));

            try
            {
                cliente.conectar();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
